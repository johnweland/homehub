$('.header .toggle').on("click", function(){
	$('.nav').toggleClass('hide');
});


function toggleDrawer() {
    $('.drawer__panel').toggleClass('closed');
    if ($('.drawer__toggle > span.fa').hasClass('fa-angle-left')) {
        $('.drawer__toggle > span.fa').removeClass('fa-angle-left');
        $('.drawer__toggle > span.fa').addClass('fa-angle-right');
    } else {
        $('.drawer__toggle > span.fa').removeClass('fa-angle-right');
        $('.drawer__toggle > span.fa').addClass('fa-angle-left');
    }
}

$('body').on("click", ".drawer__toggle", toggleDrawer);